﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FoodSense_Api.Tests.Repositories
{
    [TestClass]
    public class StorageRepositoryTests
    {
        private UnitTestApplicationDbContext _testApplicationDbContext;
        private IRepository<Storage> _storageRepository;
        private Storage _testStorage;

        [TestInitialize]
        public void InitializeStorageRepoTests()
        {
            _testApplicationDbContext = new UnitTestApplicationDbContext(Effort.DbConnectionFactory.CreateTransient());
            _storageRepository = new StorageRepository(_testApplicationDbContext);

            _testStorage = GetTestStorage();
            _storageRepository.Insert(_testStorage);
            _storageRepository.Save();
        }

        [TestMethod]
        public void GetById_ShouldReturn_Storage()
        {
            Storage actualStorage = _storageRepository.GetById(1);
            Assert.AreEqual(_testStorage, actualStorage);
        }

        [TestMethod]
        public void Get_ShouldReturn_Storage()
        {
            Storage actualStorage = _storageRepository.Get(s => s.Name == _testStorage.Name);
            Assert.AreEqual(_testStorage, actualStorage);
        }

        [TestMethod]
        public void Get_ShouldReturn_Null_AfterDelete()
        {
            _storageRepository.Delete(_testStorage);

            Storage actualStorage = _storageRepository.Get(s => s.Name == _testStorage.Name);
            Assert.IsNotNull(actualStorage);
        }

        private Storage GetTestStorage()
        {
            return new Storage()
            {
                Id = 1,
                Name = "test1"
            };
        }
    }
}
