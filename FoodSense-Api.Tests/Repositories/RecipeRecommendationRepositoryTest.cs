﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Repositories.Implementations;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace FoodSense_Api.Tests.Repositories
{
    /// <summary>
    /// Summary description for RecipeRecommendationRepositoryTest
    /// </summary>
    [TestClass]
    public class RecipeRecommendationRepositoryTest
    {
        private UnitTestApplicationDbContext _testApplicationDbContext;
        private IRepository<RecipeRating> _recipeRecommendationRepository;
        private IRepository<Recipe> _recipeRepository;
        private IRepository<Product> _productRepository;

        public RecipeRecommendationRepositoryTest()
        {
            _testApplicationDbContext = new UnitTestApplicationDbContext(Effort.DbConnectionFactory.CreateTransient());
            _recipeRecommendationRepository = new RecipeRecommendationRepository(_testApplicationDbContext);
            _recipeRepository = new RecipeRepository(_testApplicationDbContext);
            _productRepository = new ProductRepository(_testApplicationDbContext);


            setInitialData();
        }

        private void setInitialData()
        {
            Product productA = new Product()
            {
                Id = 1,
                EAN = "12345",
                Name = "TestProduct1",
                Carbs = 101,
                Protein = 101,
                Fats = 101,
                Weight = 101,
                Type = "Grams"

            };
            _productRepository.Insert(productA);
            Product productB = new Product()
            {
                Id = 2,
                EAN = "54321",
                Name = "TestProduct2",
                Carbs = 10,
                Protein = 10,
                Fats = 10,
                Weight = 10,
                Type = "Grams"
            };
            _productRepository.Insert(productB);
            Recipe recipeA = new Recipe()
            {
                Id = 2,
                Name = "rec b",
                Ingredients = new List<Product>() { productA, productB }
            };
            _recipeRepository.Insert(recipeA);
            //init recipe ratins
            RecipeRating recipeRatingA = new RecipeRating()
            {
                Rating = 5,
                Recipe = recipeA,
                Id = 1,
                User = new ApplicationUser() { Name = "a", Id = "test", UserName = "testuser" }
            };
            _recipeRecommendationRepository.Insert(recipeRatingA);
            _recipeRecommendationRepository.Save();
        }

        [TestMethod]
        public void testRecipeRatingGetAll()
        {
            ICollection<RecipeRating> recipeRatings = _recipeRecommendationRepository.GetAll(r => r.User.Id == "test");
            List<RecipeRating> ratings = (List<RecipeRating>)recipeRatings;
            Assert.AreNotEqual(ratings[0].Recipe, null);
            Assert.AreNotEqual(ratings[0].User, null);
            Assert.AreEqual(ratings[0].Id, 1);
        }

        [TestMethod]
        public void testRecipeRatingUpdate()
        {
            RecipeRating rating = _recipeRecommendationRepository.GetById(1);
            rating.Rating = 4;
            _recipeRecommendationRepository.Update(rating);
            _recipeRecommendationRepository.Save();
            rating = _recipeRecommendationRepository.GetById(1);
            Assert.AreEqual(rating.Rating, 4);
        }
    }
}
