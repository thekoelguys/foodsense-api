﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FoodSense_Api.Tests.Repositories
{
    [TestClass]
    public class ShoppinglistRepositoryTests
    {
        private UnitTestApplicationDbContext _testApplicationDbContext;
        private IRepository<Shoppinglist> _shoppinglistRepository;
        private Shoppinglist _testShoppinglist;

        [TestInitialize]
        public void InitializeShoppinglistRepoTests()
        {
            _testApplicationDbContext = new UnitTestApplicationDbContext(Effort.DbConnectionFactory.CreateTransient());
            _shoppinglistRepository = new ShoppingListRepository(_testApplicationDbContext);

            _testShoppinglist = GetTestShoppinglist();
            _shoppinglistRepository.Insert(_testShoppinglist);
            _shoppinglistRepository.Save();
        }

        [TestMethod]
        public void GetById_ShouldReturn_Shoppinglist()
        {
            Shoppinglist actualShoppinglist = _shoppinglistRepository.GetById(1);
            Assert.AreEqual(_testShoppinglist, actualShoppinglist);
        }

        [TestMethod]
        public void Get_ShouldReturn_Shoppinglist()
        {
            Shoppinglist actualShoppinglist = _shoppinglistRepository.Get(s => s.Name == _testShoppinglist.Name);
            Assert.AreEqual(_testShoppinglist, actualShoppinglist);
        }

        [TestMethod]
        public void Get_ShouldReturn_Null_AfterDelete()
        {
            _shoppinglistRepository.Delete(_testShoppinglist);

            Shoppinglist actualShoppinglist = _shoppinglistRepository.Get(s => s.Name == _testShoppinglist.Name);
            Assert.IsNotNull(actualShoppinglist);
        }

        private Shoppinglist GetTestShoppinglist()
        {
            return new Shoppinglist()
            {
                Id = 1,
                Name = "test1"
            };
        }
    }
}
