﻿using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodSense_Api.Tests.Repositories
{
    [TestClass]
    public class RecipeRepositoryTests
    {

        private UnitTestApplicationDbContext _testApplicationDbContext;
        private readonly IRepository<Recipe> _recipeRepository;

        public RecipeRepositoryTests()
        {
            _testApplicationDbContext = new UnitTestApplicationDbContext(Effort.DbConnectionFactory.CreateTransient());
            _recipeRepository = new RecipeRepository(_testApplicationDbContext);

            setInitialData();
        }

        private void setInitialData()
        {
            _recipeRepository.Insert(new Recipe()
            {
                Id = 1,
                Comment = "test recipe",
                Ingredients = null,
                Instructions = "Geen",
                Name = "test"
            });

            _recipeRepository.Save();
        }

        [TestMethod]
        public void testGetRecipeById()
        {
            Recipe recipe = _recipeRepository.GetById(1);
            Assert.AreEqual(1, recipe.Id);
        }

        [TestMethod]
        public void testDeleteRecipe()
        {
            Recipe recipe = _recipeRepository.GetById(1);
            Assert.AreNotEqual(null, recipe);
            _recipeRepository.Delete(recipe);
            _recipeRepository.Save();

            var r = _recipeRepository.GetById(1);

            Assert.AreEqual(null, r);
        }

        [TestMethod]
        public void testInsertingNewRecipe()
        {
            Recipe recipe = new Recipe()
            {
                Id = 2,
                Comment = "test",
                Ingredients = null,
                Instructions = "",
                Name = "test"
            };
            _recipeRepository.Insert(recipe);
            _recipeRepository.Save();

            var recipeFromDB = _recipeRepository.GetById(2);
            Assert.AreEqual(recipe, recipeFromDB);
        }
    }
}
