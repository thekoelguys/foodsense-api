﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Common;

namespace FoodSense_Api.Tests.Repositories
{
    [TestClass]
    public class ProductRepositoryTests
    {
        private UnitTestApplicationDbContext _testApplicationDbContext;
        private IRepository<Product> _productRepository;
        private Product _testProduct;

        [TestInitialize]
        public void InitializeProductRepoTests()
        {
            _testApplicationDbContext = new UnitTestApplicationDbContext(Effort.DbConnectionFactory.CreateTransient());
            _productRepository = new ProductRepository(_testApplicationDbContext);

            _testProduct = GetTestProduct();
            _productRepository.Insert(_testProduct);
            _productRepository.Save();
        }

        [TestMethod]
        public void GetById_ShouldReturn_Product()
        {
            Product actualProduct = _productRepository.GetById(1);
            Assert.AreEqual(_testProduct, actualProduct);
        }

        [TestMethod]
        public void Get_ShouldReturn_Product()
        {
            Product actualProduct = _productRepository.Get(p => p.EAN == _testProduct.EAN);
            Assert.AreEqual(_testProduct, actualProduct);
        }

        [TestMethod]
        public void Get_ShouldReturn_Null_AfterDelete()
        {
            _productRepository.Delete(_testProduct);

            Product actualProduct = _productRepository.Get(p => p.EAN == _testProduct.EAN);
            Assert.IsNotNull(actualProduct);
        }

        private Product GetTestProduct()
        {
            return new Product()
            {
                Id = 1,
                EAN = "12345",
                Name = "TestProduct",
                Carbs = 10,
                Protein = 10,
                Fats = 10,
                Weight = 10,
                Type = "Grams"

            };
        }

    }
}
