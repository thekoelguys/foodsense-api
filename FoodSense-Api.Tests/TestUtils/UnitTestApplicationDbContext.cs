﻿using System.Data.Common;
using FoodSense_Api.Models.Context;

namespace FoodSense_Api.Tests
{
    public class UnitTestApplicationDbContext : BaseApplicationDbContext
    {

        public UnitTestApplicationDbContext(DbConnection connection) 
        : base(connection)
        {
        }
    }
}