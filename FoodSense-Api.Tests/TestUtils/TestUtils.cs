﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;

namespace FoodSense_Api.Tests.TestUtils
{
    public class TestUtils
    {
        public static Product GetTestProduct()
        {
            return new Product()
            {
                Id = 1,
                EAN = "12345",
                Name = "TestProduct",
                Carbs = 10,
                Protein = 10,
                Fats = 10,
                Weight = 10,
                Type = "Grams"

            };
        }

    }
}
