﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using FoodSense_Api.Controllers;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FoodSense_Api.Tests.Controllers
{
    [TestClass]
    public class ProductControllerTests
    {
        private UnitTestApplicationDbContext _context;
        private IRepository<Product> _productRepository;
        private Product _testProduct;

        [TestInitialize]
        public void Initialize()
        {
            _context           = new UnitTestApplicationDbContext(Effort.DbConnectionFactory.CreateTransient());
            _productRepository = new ProductRepository(_context);
            _testProduct       = TestUtils.TestUtils.GetTestProduct();

            _productRepository.Insert(_testProduct);
            _productRepository.Save();
        }

        [TestMethod]
        public void GetByEan_ShouldReturn_Product()
        {
            ProductController controller = new ProductController(_context);
            IHttpActionResult actualResult = controller.GetByEan("12345");

            Assert.IsInstanceOfType(actualResult, typeof(OkNegotiatedContentResult<Product>));
        }

        [TestMethod]
        public void GetByEan_ShouldReturn_404()
        {
            ProductController controller   = new ProductController(_context);
            IHttpActionResult actualResult = controller.GetByEan("unknownEan");

            Assert.IsInstanceOfType(actualResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void Post_ShouldReturn_OkResultWithProduct()
        {
            ProductController controller   = new ProductController(_context);
            IHttpActionResult actualResult = controller.Post(new Product()
            {
                Id = 2,
                EAN = "newEan",
                Name = "TestProduct2",
                Carbs = 10,
                Protein = 10,
                Fats = 10,
                Weight = 10,
                Type = "Grams"
            });

            Assert.IsInstanceOfType(actualResult, typeof(OkNegotiatedContentResult<Product>));
        }

        [TestMethod]
        public void Post_ShouldReturn_409()
        {
            ProductController controller   = new ProductController(_context);
            IHttpActionResult actualResult = controller.Post(_testProduct);

            Assert.IsInstanceOfType(actualResult, typeof(ConflictResult));
        }
    }
}
