﻿using FoodSense_Api.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RecommendationSystem;
using RecommendationSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodSense_Api.Tests.Controllers
{
    [TestClass]
    public class RecommendationServiceTest
    {
        [TestMethod]
        public void EmptyRecommendationTest()
        {
            RecipeRecommendationService recommendations = new RecipeRecommendationService();
            ICollection<RecipeRating> recipeRatings = new List<RecipeRating>();
            var result = recommendations.GetRecipesForStorage(recipeRatings, new List<StoragesProducts>());
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void RatingWithoutRecipeRecommendationTest()
        {
            RecipeRecommendationService recommendations = new RecipeRecommendationService();
            ICollection<RecipeRating> recipeRatings = new List<RecipeRating>();
            recipeRatings.Add(new RecipeRating()
            {
                Rating = 5,
                Recipe = null,
                User = null,
                Id = 1
            });
            var result = recommendations.GetRecipesForStorage(recipeRatings, new List<StoragesProducts>());
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void FilledRecommendationTest()
        {
            RecipeRecommendationService recommendations = new RecipeRecommendationService();
            ICollection<RecipeRating> recipeRatings = new List<RecipeRating>();

            //init products
            Product productA = new Product()
            {
                Id = 1,
                Name = "test"            
            };
            Product productB = new Product()
            {
                Id = 2,
                Name = "test2"
            };

            //init recipes
            Recipe recipeA = new Recipe()
            {
                Id = 1,
                Name = "rec a",
                Ingredients = new List<Product>() { productA }
            };

            Recipe recipeB = new Recipe()
            {
                Id = 2,
                Name = "rec b",
                Ingredients = new List<Product>() { productA, productB }
            };

            //init recipe ratins
            RecipeRating recipeRatingA = new RecipeRating()
            {
                Rating = 5,
                Recipe = recipeB,
                User = null,
                Id = 1
            };

            RecipeRating recipeRatingB = new RecipeRating()
            {
                Rating = 3,
                Recipe = recipeA,
                User = null,
                Id = 1
            };
            //add recipes to corresponding product
            productA.Recipes = new List<Recipe>();
            productA.Recipes.Add(recipeA);
            productA.Recipes.Add(recipeB);
            productB.Recipes = new List<Recipe>();
            productB.Recipes.Add(recipeB);

            //add recipe ratings to the list of recipe ratings
            recipeRatings.Add(recipeRatingA);
            recipeRatings.Add(recipeRatingB);

            // add product to stan storage
            ICollection<StoragesProducts> storagesProducts = new List<StoragesProducts>();
            storagesProducts.Add(new StoragesProducts()
            {
                ProductId = 1,
                Product = productA
            });


            var result = recommendations.GetRecipesForStorage(recipeRatings, storagesProducts);
            Assert.AreEqual(recipeA, result.First());
            Assert.AreEqual(2, result.Count);
        }
    }
}
