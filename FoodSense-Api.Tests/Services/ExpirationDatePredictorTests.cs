﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Common;
using FoodSense_Api.Repositories;

namespace FoodSense_Api.Tests.Services
{
    [TestClass]
    public class ExpirationDatePredictorTests
    {
        private UnitTestApplicationDbContext _context;
        private IRepository<StoragesProducts> _storageProductRepository;
        private Product _testProduct;

        [TestInitialize]
        public void Initialize()
        {
            _context = new UnitTestApplicationDbContext(Effort.DbConnectionFactory.CreateTransient());
            _storageProductRepository = new StoragesProductsRepository(_context);
            _testProduct = GetTestProduct();

            InsertTestData();
        }

        [TestMethod]
        public void Predictor_Should_Return_ExpectedDate()
        {
            ExpirationDatePredictor predictor = new ExpirationDatePredictor(_storageProductRepository);

            DateTime expectedDate = DateTime.Now.AddDays(15);
            DateTime result       = predictor.PredictExpirationDate(_testProduct);

            Assert.AreEqual(expectedDate.Day, result.Day);
            Assert.AreEqual(expectedDate.Year, result.Year);
            Assert.AreEqual(expectedDate.Month, result.Month);
        }

        private Product GetTestProduct()
        {
            return new Product()
            {
                Id = 1,
                EAN = "12345",
                Name = "TestProduct",
                Carbs = 10,
                Protein = 10,
                Fats = 10,
                Weight = 10,
                Type = "Grams"

            };
        }

        private void InsertTestData()
        {
            var productRepo = new ProductRepository(_context);
            var storageRepo = new StorageRepository(_context);

            // Insert products.
            productRepo.Insert(GetTestProduct());
            productRepo.Save();

            // Insert storages.
            var testStorage = new Storage("test");
            storageRepo.Insert(new Storage()
            {
                Id = 1,
                Name = "test1"
            });

            storageRepo.Save();

            // Insert storageProducts
            var sp1 = new StoragesProducts(testStorage, _testProduct, DateTime.Now.AddDays(10));
            var sp2 = new StoragesProducts(testStorage, _testProduct, DateTime.Now.AddDays(20));
            _storageProductRepository.Insert(sp1);
            _storageProductRepository.Insert(sp2);
            _storageProductRepository.Save();

        }
    }
}
