# FoodSense-Api #

[![Build status](https://ci.appveyor.com/api/projects/status/u3xtjrhv0pw348cu/branch/master?svg=true)](https://ci.appveyor.com/project/erikeuserr/foodsense-api/branch/master)
[![Coverage Status](https://coveralls.io/repos/bitbucket/thekoelguys/foodsense-api/badge.svg?branch=master)](https://coveralls.io/bitbucket/thekoelguys/foodsense-api?branch=master)
Before running -> Install NuGet packages.

## Documentation ##

Documentation of the Api is available at http://localhost:{yourPort}/swagger.
You can test the Api in at this link too.

## Authentication ##

Authentication is based on OAuth2.0. All calls need Authentication, except the Login and Register calls. 

**Client**

For calls that need authentication: use an Authorization header with: Bearer {token} as value. (space inbetween Bearer and token).

**Retrieving an OAuth token**

Retrieving an OAuth token is done by doing a POST call to
http://localhost:{yourPort}/api/login.

**Headers**

Content-Type : application/x-www-form-urlencoded

**Request body**

username={UserNameorEmailAddress}&password={YourPassword}&grant_type=password