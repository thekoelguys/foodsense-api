﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class UserAchievement
    {
        public UserAchievement()
        {
            DateAchieved = DateTime.Now;
        }

        public UserAchievement(ApplicationUser user, Achievement achievement)
        {
            User = user;
            Achievement = achievement;
            DateAchieved = DateTime.Now;
        }

        [JsonIgnore]
        public int Id { get; set; }
        
        [JsonIgnore]
        public DateTime DateAchieved { get; set; }

        [NotMapped]
        public string DateAchievedString => DateAchieved.ToString("yyyy-MM-dd");

        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }
        public virtual Achievement Achievement { get; set; }

        [JsonIgnore]
        public virtual string UserId { get; set; }

        [JsonIgnore]
        public virtual int AchievementId { get; set; }
    }
}