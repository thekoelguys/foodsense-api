﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class StoragesProducts
    {
        public StoragesProducts()
        {
        }



        public StoragesProducts(Storage storage, Product product, DateTime expirationDate)
        {
            Storage = storage;
            Product = product;
            DateAdded = DateTime.Now;
            Weight = product.Weight;
            WeightType = product.Type;
            ProductId = product.Id;
            StorageId = storage.Id;
            ExpirationDate = expirationDate;
        }

        [Key]
        public int Id { get; set; }

        public double? Weight { get; set; }

        public string WeightType { get; set; }

        [JsonIgnore]
        public DateTime DateAdded { get; set; }

        [NotMapped]
        public string DateAddedString => DateAdded.ToString("yyyy-MM-dd");

        [JsonIgnore]
        public DateTime ExpirationDate { get; set; }

        [NotMapped]
        public string ExpirationDateString => ExpirationDate.ToString("yyyy-MM-dd");

        public virtual int StorageId { get; set; }

        public virtual int ProductId { get; set; }

        [JsonIgnore]
        public virtual Storage Storage { get; set; }

        public virtual Product Product { get; set; }
    }
}