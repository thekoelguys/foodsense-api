﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class ShoppinglistProduct
    {
        public ShoppinglistProduct() { }
        public ShoppinglistProduct(Product product, Shoppinglist shoppinglist)
        {
            Product = product;
            Shoppinglist = shoppinglist;
        }

        [Key]
        public int Id { get; set; }

        public virtual int ShoppingListId { get; set; }

        public virtual int ProductId { get; set; }

        public virtual Product Product { get; private set; }

        [JsonIgnore]
        public virtual Shoppinglist Shoppinglist { get; private set; }
    }
}