﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FoodSense_Api.Enums;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class Achievement
    {
        public Achievement()
        {
            UserAchievements = new List<UserAchievement>();
        }

        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public AchievementType AchievementType { get; set; }

        public int Points { get; set; }

        [JsonIgnore]
        public ICollection<UserAchievement> UserAchievements { get; set; } 
    }
}