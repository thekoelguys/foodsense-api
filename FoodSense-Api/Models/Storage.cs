﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{

    public class Storage
    {

        public Storage()
        {
            
        }

        public Storage(string registrationToken)
        {
            RegistrationToken = registrationToken;
            ConnectionToken   = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
        }

        [Key]
        public int Id { get; set; }

        [JsonIgnore]
        public string RegistrationToken { get; set; }

        public string Name { get; set; }

        public int Humidity { get; set; }

        public int Temperature { get; set; }

        public int Weight { get; set; }

        [NotMapped]
        public int ProductCount => StorageProducts.Count;

        [JsonIgnore]
        public string ConnectionToken { get; set; }

        [JsonIgnore]
        public virtual ICollection<ApplicationUser> Users { get; set; }

        [JsonIgnore]
        public virtual ICollection<StoragesProducts> StorageProducts { get; set; }

        [JsonIgnore]
        public virtual ICollection<StorageHistory> StorageHistories { get; set; }

    }
}