﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSense_Api.Models
{
    public class StorageInfo
    {
        public int NumberOfProducts { get; set; }
        public DateTime firstExpiringProductDate { get; set; }
        public Product firstExpiringProduct { get; set; }
    }
}