﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSense_Api.Models
{
    public class RecipeRating
    {
        public int Id { get; set; }
        public int Rating
        {
            get;set;
            /*get
            {
                return Rating;
            }
            set
            {
                if(value >= 0 && value <= 5)
                {
                    Rating = value;
                }
                else
                {
                    //throw new ArgumentException("The Rating must be between 0 and 5");\
                    Rating = 0;
                }
            }*/
        }
        public ApplicationUser User { get; set; }
        public Recipe Recipe { get; set; }
    }
}