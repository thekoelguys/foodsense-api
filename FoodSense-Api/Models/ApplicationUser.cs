﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.ModelBinding;
using FoodSense_Api.Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        public DateTime DateCreated { get; private set; }

        public ICollection<RecipeRating> RecipeRatings { get; set; }

        [JsonIgnore]
        public virtual ICollection<Shoppinglist> Shoppinglists { get; set; }

        [JsonIgnore]
        public virtual ICollection<Storage> Storages {get; set; }

        [JsonIgnore]
        public virtual ICollection<ApplicationUser> Friends { get; set; }

        [JsonIgnore]
        public virtual ICollection<UserAchievement> UserAchievements { get; set; }

        [JsonIgnore]
        public int RecipeRatedCount { get; set; }

        [JsonIgnore]
        public int AddedProductCount { get; set; }

        [JsonIgnore]
        public int DeletedProductInTimeCount { get; set; }

        [NotMapped]
        public int TotalPoints => RecipeRatedCount + AddedProductCount + DeletedProductInTimeCount;


        public ApplicationUser()
        {
            DateCreated = DateTime.Now;
            Storages = new List<Storage>();
            Shoppinglists = new List<Shoppinglist>();
            UserAchievements = new List<UserAchievement>();
        }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);

            return userIdentity;
        }

        public Profile GetProfile(bool showFriends)
        {
            return showFriends 
                ? new Profile(Id, Name, Age, Sex, Email, Friends, UserAchievements, TotalPoints)
                : new Profile(Id, Name, Age, Sex, Email, UserAchievements, TotalPoints);
        }
    }
}