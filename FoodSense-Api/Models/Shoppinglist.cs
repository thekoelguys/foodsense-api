﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class Shoppinglist
    {
        public Shoppinglist(ApplicationUser user, string name)
        {
            User = user;
            Name = name;
            DateCreated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public Shoppinglist()
        {
            ShoppinglistsProducts = new List<ShoppinglistProduct>();
            DateCreated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string DateCreated { get; private set; }

        [JsonIgnore]
        public virtual ICollection<ShoppinglistProduct> ShoppinglistsProducts { get; set; }

        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }


        
    }
}