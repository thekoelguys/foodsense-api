﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string EAN { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public double? Weight { get; set; }

        [Required]
        public string Type { get; set; }

        /// <summary>
        /// Protein in grams per 100 grams/ml of Weight.
        /// </summary>
        [Required]
        public double Protein { get; set; }

        /// <summary>
        /// Carbs in grams per 100 grams/ml of Weight.
        /// </summary>
        [Required]
        public double Carbs { get; set; }

        /// <summary>
        /// Fats in grams per 100 grams/ml of Weight.
        /// </summary>
        [Required]
        public double Fats { get; set; }

        [JsonIgnore]
        public virtual ICollection<StoragesProducts> StorageProducts { get; set; }

        [JsonIgnore]
        public virtual ICollection<ShoppinglistProduct> ShoppinglistsProducts { get; set; }

        [JsonIgnore]
        public ICollection<Recipe> Recipes { get; set; }

        public Product()
        {
            ShoppinglistsProducts = new List<ShoppinglistProduct>();
            StorageProducts = new List<StoragesProducts>();
        }
    }
}