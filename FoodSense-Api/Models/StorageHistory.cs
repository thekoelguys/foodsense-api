﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class StorageHistory
    {
        public StorageHistory() { }
        public StorageHistory(int humidity, int temperature, int weight ,Storage storage)
        {
            Humidity = humidity;
            Temperature = temperature;
            Weight = weight;
            Timestamp = DateTime.Now;
            Storage = storage;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public int Humidity { get; set; }

        [Required]
        public int Temperature { get; set; }

        public int Weight { get; set; }

        public DateTime Timestamp { get; set; }
        
        [JsonIgnore]
        public virtual Storage Storage { get; set; }
    }

}