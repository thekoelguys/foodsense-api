﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FoodSense_Api.Models
{
    public class Recipe
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Instructions { get; set; }

        public string Comment { get; set; }

        public double AverageRating { get; set; }

        public ICollection<Product> Ingredients { get; set; }

        [JsonIgnore]
        public ICollection<RecipeRating> RecipeRatings { get; set; } 
    }
}