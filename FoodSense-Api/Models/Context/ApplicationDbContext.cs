﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using FoodSense_Api.Models.Context;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FoodSense_Api.Models
{
    public class ApplicationDbContext : BaseApplicationDbContext
    {

#if DEBUG
        public ApplicationDbContext()
            : base("DebugConnectionString", throwIfV1Schema: false)
        {
        }
#else
                public ApplicationDbContext()
                    : base("ProductionConnectionString", throwIfV1Schema: false)
                {
                }
#endif
    }
}