﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Common;

namespace FoodSense_Api.Models.Context
{
    public abstract class BaseApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<StorageHistory> StorageHistories { get; set; }
        public virtual DbSet<Recipe> Recipes { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Storage> Storages { get; set; }
        public virtual DbSet<StoragesProducts> StoragesProducts { get; set; }
        public virtual DbSet<RecipeRating> RecipeRating { get; set; }
        public virtual DbSet<Shoppinglist> Shoppinglists { get; set; }
        public virtual DbSet<ShoppinglistProduct> ShoppinglistProducts { get; set; }

        protected BaseApplicationDbContext(string connectionstring, bool throwIfV1Schema)
            : base(connectionstring, throwIfV1Schema: throwIfV1Schema)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public BaseApplicationDbContext(DbConnection connection) 
        : base(connection, true)
        {

        }
    }
}