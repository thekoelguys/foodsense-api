﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodSense_Api.Models
{
    [NotMapped]
    public class Profile
    {
        public Profile(string id, string name, int age, string sex, string emailAddress, ICollection<ApplicationUser> friends, ICollection<UserAchievement> userAchievements, int points)
        {
            Id = id;
            Name = name;
            Age = age;
            Sex = sex;
            EmailAddress = emailAddress;
            UserAchievements = userAchievements;

            Friends = new List<Profile>();

            foreach (var friend in friends)
            {
                Friends.Add(friend.GetProfile(false));
            }

            Points = points;
        }

        public Profile(string id, string name, int age, string sex, string emailAddress, ICollection<UserAchievement> userAchievements, int totalPoints)
        {
            Id = id;
            Name = name;
            Age = age;
            Sex = sex;
            EmailAddress = emailAddress;
            UserAchievements = userAchievements;
            Points = totalPoints;
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public int Age { get; private set; }
        public string Sex { get; private set; }
        public string EmailAddress { get; private set; }
        public ICollection<UserAchievement> UserAchievements { get; private set; }
        public List<Profile> Friends { get; private set; }
        public int Points { get; private set; }
    }
}