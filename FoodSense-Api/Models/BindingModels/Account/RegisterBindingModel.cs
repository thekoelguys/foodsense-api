﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using FoodSense_Api.Enums;

namespace FoodSense_Api.Models.BindingModels
{
    public class RegisterBindingModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Only positive numbers are allowed.")]
        [Display(Name = "Age")]
        public int Age { get; set; }

        [Required]
        [RegularExpression("Male|Female", ErrorMessage = "Sex can only be Male or Female.")]
        public string Sex { get; set; }

        [Required]
        public string Name { get; set; }
    }
}