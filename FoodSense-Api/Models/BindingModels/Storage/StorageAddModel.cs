﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FoodSense_Api.Models.BindingModels.Storage
{
    public class StorageAddModel
    {
        [Required] public string Token { get; set; }
        [Required] public string Name { get; set; }
    }
}