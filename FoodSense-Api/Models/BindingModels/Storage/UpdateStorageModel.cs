﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FoodSense_Api.Models.BindingModels.Storage
{
    public class UpdateStorageModel
    {
        public int StorageId { get; set; }

        [Required]
        public int Humidity { get; set; }

        [Required]
        public int Temperature { get; set; }

        [Required]
        public int Weight { get; set; }
    }
}