﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FoodSense_Api.Models.BindingModels.Friend
{
    public class FriendRequestModel
    {
        [Required]
        public string UserId { get; set; }
    }
}