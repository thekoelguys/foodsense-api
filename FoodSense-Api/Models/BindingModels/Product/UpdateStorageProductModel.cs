﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FoodSense_Api.Models.BindingModels.Product
{
    public class UpdateStorageProductModel
    {
        [Required]
        [DataType(DataType.Date)]
        public string ExpirationDate { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Weight { get; set; }

        [Required]
        public string WeightType { get; set; }
    }
}