﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace FoodSense_Api.Models.BindingModels.Product
{
    public class AddProductModel
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int StorageId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int ProductId { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int Amount { get; set; }

        [Required]
        [RegularExpression("^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$")]
        public string ExpirationDate { get; set; }
    }
}