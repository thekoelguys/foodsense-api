﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSense_Api.Models.BindingModels.Shoppinglist
{
    public class ExportShoppinglistModel
    {
        public int StorageId { get; set; }

        public int[] Products { get; set; }
    }
}