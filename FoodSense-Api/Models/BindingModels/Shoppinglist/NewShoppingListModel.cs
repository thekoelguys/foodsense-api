﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FoodSense_Api.Models.BindingModels.Shoppinglist
{
    public class NewShoppingListModel
    {
        [Required]
        public string Name { get; set; }
    }
}