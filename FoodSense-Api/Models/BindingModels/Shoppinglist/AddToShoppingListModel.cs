﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FoodSense_Api.Models.BindingModels.Product;

namespace FoodSense_Api.Models.BindingModels.Shoppinglist
{
    /// <summary>
    /// Post-body model for adding a product to a shoppinglist.
    /// </summary>
    public class AddToShoppingListModel
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int ShoppingListId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int ProductId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Amount { get; set; }
    }
}