namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StoragesProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.StoragesProducts", "Storage_Id", "dbo.Storages");
            DropIndex("dbo.StoragesProducts", new[] { "Product_Id" });
            DropIndex("dbo.StoragesProducts", new[] { "Storage_Id" });
            RenameColumn(table: "dbo.StoragesProducts", name: "Product_Id", newName: "ProductId");
            RenameColumn(table: "dbo.StoragesProducts", name: "Storage_Id", newName: "StorageId");
            AlterColumn("dbo.StoragesProducts", "ProductId", c => c.Int(nullable: false));
            AlterColumn("dbo.StoragesProducts", "StorageId", c => c.Int(nullable: false));
            CreateIndex("dbo.StoragesProducts", "StorageId");
            CreateIndex("dbo.StoragesProducts", "ProductId");
            AddForeignKey("dbo.StoragesProducts", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.StoragesProducts", "StorageId", "dbo.Storages", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StoragesProducts", "StorageId", "dbo.Storages");
            DropForeignKey("dbo.StoragesProducts", "ProductId", "dbo.Products");
            DropIndex("dbo.StoragesProducts", new[] { "ProductId" });
            DropIndex("dbo.StoragesProducts", new[] { "StorageId" });
            AlterColumn("dbo.StoragesProducts", "StorageId", c => c.Int());
            AlterColumn("dbo.StoragesProducts", "ProductId", c => c.Int());
            RenameColumn(table: "dbo.StoragesProducts", name: "StorageId", newName: "Storage_Id");
            RenameColumn(table: "dbo.StoragesProducts", name: "ProductId", newName: "Product_Id");
            CreateIndex("dbo.StoragesProducts", "Storage_Id");
            CreateIndex("dbo.StoragesProducts", "Product_Id");
            AddForeignKey("dbo.StoragesProducts", "Storage_Id", "dbo.Storages", "Id");
            AddForeignKey("dbo.StoragesProducts", "Product_Id", "dbo.Products", "Id");
        }
    }
}
