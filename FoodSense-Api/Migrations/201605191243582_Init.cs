namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    // Init Migration.

    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EAN = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Weight = c.Double(nullable: false),
                        Protein = c.Double(nullable: false),
                        Carbs = c.Double(nullable: false),
                        Fats = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Recipes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Instructions = c.String(),
                        Comment = c.String(),
                        AverageRating = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ShoppinglistProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShoppingListId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Shoppinglists", t => t.ShoppingListId, cascadeDelete: true)
                .Index(t => t.ShoppingListId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Shoppinglists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateCreated = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Age = c.Int(nullable: false),
                        Sex = c.String(),
                        HouseHoldSize = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.RecipeRatings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rating = c.Int(nullable: false),
                        Recipe_Id = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.Recipe_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Recipe_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Storages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegistrationToken = c.String(),
                        Name = c.String(),
                        Humidity = c.Int(nullable: false),
                        Temperature = c.Int(nullable: false),
                        ConnectionToken = c.String()  ,                      
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StoragesProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StorageId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        DateAdded = c.String(),
                        ExpirationDate = c.String(),
                    Weight = c.Double(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Storages", t => t.StorageId, cascadeDelete: true)
                .Index(t => t.StorageId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.StorageHistories",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Humidity = c.Int(nullable: false),
                        Temperature = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Storage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Storages", t => t.Storage_Id)
                .Index(t => t.Storage_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.RecipeProducts",
                c => new
                    {
                        Recipe_Id = c.Int(nullable: false),
                        Product_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Recipe_Id, t.Product_Id })
                .ForeignKey("dbo.Recipes", t => t.Recipe_Id, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.Product_Id, cascadeDelete: true)
                .Index(t => t.Recipe_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.StorageApplicationUsers",
                c => new
                    {
                        Storage_Id = c.Int(nullable: false),
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Storage_Id, t.ApplicationUser_Id })
                .ForeignKey("dbo.Storages", t => t.Storage_Id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .Index(t => t.Storage_Id)
                .Index(t => t.ApplicationUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.StorageApplicationUsers", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.StorageApplicationUsers", "Storage_Id", "dbo.Storages");
            DropForeignKey("dbo.StorageHistories", "Storage_Id", "dbo.Storages");
            DropForeignKey("dbo.StoragesProducts", "StorageId", "dbo.Storages");
            DropForeignKey("dbo.StoragesProducts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Shoppinglists", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RecipeRatings", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.RecipeRatings", "Recipe_Id", "dbo.Recipes");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShoppinglistProducts", "ShoppingListId", "dbo.Shoppinglists");
            DropForeignKey("dbo.ShoppinglistProducts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.RecipeProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.RecipeProducts", "Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.StorageApplicationUsers", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.StorageApplicationUsers", new[] { "Storage_Id" });
            DropIndex("dbo.RecipeProducts", new[] { "Product_Id" });
            DropIndex("dbo.RecipeProducts", new[] { "Recipe_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.StorageHistories", new[] { "Storage_Id" });
            DropIndex("dbo.StoragesProducts", new[] { "ProductId" });
            DropIndex("dbo.StoragesProducts", new[] { "StorageId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.RecipeRatings", new[] { "User_Id" });
            DropIndex("dbo.RecipeRatings", new[] { "Recipe_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Shoppinglists", new[] { "User_Id" });
            DropIndex("dbo.ShoppinglistProducts", new[] { "ProductId" });
            DropIndex("dbo.ShoppinglistProducts", new[] { "ShoppingListId" });
            DropTable("dbo.StorageApplicationUsers");
            DropTable("dbo.RecipeProducts");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.StorageHistories");
            DropTable("dbo.StoragesProducts");
            DropTable("dbo.Storages");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.RecipeRatings");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Shoppinglists");
            DropTable("dbo.ShoppinglistProducts");
            DropTable("dbo.Recipes");
            DropTable("dbo.Products");
        }
    }
}
