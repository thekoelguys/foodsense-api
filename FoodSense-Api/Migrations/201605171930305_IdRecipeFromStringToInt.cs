namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdRecipeFromStringToInt : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RecipeProducts", "Recipe_Id", "dbo.Recipes");
            DropForeignKey("dbo.RecipeRatings", "Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.RecipeRatings", new[] { "Recipe_Id" });
            DropIndex("dbo.RecipeProducts", new[] { "Recipe_Id" });
            DropPrimaryKey("dbo.Recipes");
            DropPrimaryKey("dbo.RecipeProducts");
            AlterColumn("dbo.Recipes", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.RecipeRatings", "Recipe_Id", c => c.Int());
            AlterColumn("dbo.RecipeProducts", "Recipe_Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Recipes", "Id");
            AddPrimaryKey("dbo.RecipeProducts", new[] { "Recipe_Id", "Product_Id" });
            CreateIndex("dbo.RecipeRatings", "Recipe_Id");
            CreateIndex("dbo.RecipeProducts", "Recipe_Id");
            AddForeignKey("dbo.RecipeProducts", "Recipe_Id", "dbo.Recipes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RecipeRatings", "Recipe_Id", "dbo.Recipes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecipeRatings", "Recipe_Id", "dbo.Recipes");
            DropForeignKey("dbo.RecipeProducts", "Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.RecipeProducts", new[] { "Recipe_Id" });
            DropIndex("dbo.RecipeRatings", new[] { "Recipe_Id" });
            DropPrimaryKey("dbo.RecipeProducts");
            DropPrimaryKey("dbo.Recipes");
            AlterColumn("dbo.RecipeProducts", "Recipe_Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.RecipeRatings", "Recipe_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Recipes", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.RecipeProducts", new[] { "Recipe_Id", "Product_Id" });
            AddPrimaryKey("dbo.Recipes", "Id");
            CreateIndex("dbo.RecipeProducts", "Recipe_Id");
            CreateIndex("dbo.RecipeRatings", "Recipe_Id");
            AddForeignKey("dbo.RecipeRatings", "Recipe_Id", "dbo.Recipes", "Id");
            AddForeignKey("dbo.RecipeProducts", "Recipe_Id", "dbo.Recipes", "Id", cascadeDelete: true);
        }
    }
}
