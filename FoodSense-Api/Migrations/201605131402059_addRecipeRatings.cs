namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRecipeRatings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RecipeRatings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rating = c.Int(nullable: false),
                        Recipe_Id = c.String(maxLength: 128),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.Recipe_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Recipe_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecipeRatings", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.RecipeRatings", "Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.RecipeRatings", new[] { "User_Id" });
            DropIndex("dbo.RecipeRatings", new[] { "Recipe_Id" });
            DropTable("dbo.RecipeRatings");
        }
    }
}
