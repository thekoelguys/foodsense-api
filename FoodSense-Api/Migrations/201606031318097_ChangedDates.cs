namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedDates : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StoragesProducts", "DateAdded", c => c.DateTime(nullable: false));
            AlterColumn("dbo.StoragesProducts", "ExpirationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StoragesProducts", "ExpirationDate", c => c.String());
            AlterColumn("dbo.StoragesProducts", "DateAdded", c => c.String());
        }
    }
}
