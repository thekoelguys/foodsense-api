namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedWeightToStorage1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Storages", "Weight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Storages", "Weight");
        }
    }
}
