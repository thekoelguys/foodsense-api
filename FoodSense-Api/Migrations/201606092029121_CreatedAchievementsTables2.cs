namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedAchievementsTables2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserAchievements", new[] { "User_Id" });
            DropColumn("dbo.UserAchievements", "UserId");
            RenameColumn(table: "dbo.UserAchievements", name: "User_Id", newName: "UserId");
            AlterColumn("dbo.UserAchievements", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.UserAchievements", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserAchievements", new[] { "UserId" });
            AlterColumn("dbo.UserAchievements", "UserId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.UserAchievements", name: "UserId", newName: "User_Id");
            AddColumn("dbo.UserAchievements", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.UserAchievements", "User_Id");
        }
    }
}
