namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedAchievementsTables1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Achievements", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Achievements", "Name");
        }
    }
}
