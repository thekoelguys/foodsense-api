namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedAchievementsTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserAchievements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateAchieved = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        AchievementId = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Achievements", t => t.AchievementId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.AchievementId)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Achievements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        AchievementType = c.Int(nullable: false),
                        Points = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "Points", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "RecipeRatedCount", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "AddedProductCount", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "DeletedProductInTimeCount", c => c.Int(nullable: false));
            DropColumn("dbo.AspNetUsers", "HouseHoldSize");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "HouseHoldSize", c => c.Int(nullable: false));
            DropForeignKey("dbo.UserAchievements", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserAchievements", "AchievementId", "dbo.Achievements");
            DropIndex("dbo.UserAchievements", new[] { "User_Id" });
            DropIndex("dbo.UserAchievements", new[] { "AchievementId" });
            DropColumn("dbo.AspNetUsers", "DeletedProductInTimeCount");
            DropColumn("dbo.AspNetUsers", "AddedProductCount");
            DropColumn("dbo.AspNetUsers", "RecipeRatedCount");
            DropColumn("dbo.AspNetUsers", "Points");
            DropTable("dbo.Achievements");
            DropTable("dbo.UserAchievements");
        }
    }
}
