namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableWeights : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StoragesProducts", "Weight", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StoragesProducts", "Weight", c => c.Double(nullable: false));
        }
    }
}
