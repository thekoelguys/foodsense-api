namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecreateDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RecipeRatings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rating = c.Int(nullable: false),
                        Recipe_Id = c.String(maxLength: 128),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.Recipe_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Recipe_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.RecipeProducts",
                c => new
                    {
                        Recipe_Id = c.String(nullable: false, maxLength: 128),
                        Product_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Recipe_Id, t.Product_Id })
                .ForeignKey("dbo.Recipes", t => t.Recipe_Id, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.Product_Id, cascadeDelete: true)
                .Index(t => t.Recipe_Id)
                .Index(t => t.Product_Id);
            
            AddColumn("dbo.Storages", "RegistrationToken", c => c.String());
            AddColumn("dbo.Storages", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecipeRatings", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.RecipeRatings", "Recipe_Id", "dbo.Recipes");
            DropForeignKey("dbo.RecipeProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.RecipeProducts", "Recipe_Id", "dbo.Recipes");
            DropIndex("dbo.RecipeProducts", new[] { "Product_Id" });
            DropIndex("dbo.RecipeProducts", new[] { "Recipe_Id" });
            DropIndex("dbo.RecipeRatings", new[] { "User_Id" });
            DropIndex("dbo.RecipeRatings", new[] { "Recipe_Id" });
            DropColumn("dbo.Storages", "Name");
            DropColumn("dbo.Storages", "RegistrationToken");
            DropTable("dbo.RecipeProducts");
            DropTable("dbo.RecipeRatings");
        }
    }
}
