namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedShoppingLists : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShoppinglistProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShoppingListId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShoppinglistProducts", "ProductId", "dbo.Products");
            DropIndex("dbo.ShoppinglistProducts", new[] { "ProductId" });
            DropTable("dbo.ShoppinglistProducts");
        }
    }
}
