namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserShoppingListRel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Shoppinglists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateCreated = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateIndex("dbo.ShoppinglistProducts", "ShoppingListId");
            AddForeignKey("dbo.ShoppinglistProducts", "ShoppingListId", "dbo.Shoppinglists", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Shoppinglists", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShoppinglistProducts", "ShoppingListId", "dbo.Shoppinglists");
            DropIndex("dbo.Shoppinglists", new[] { "User_Id" });
            DropIndex("dbo.ShoppinglistProducts", new[] { "ShoppingListId" });
            DropTable("dbo.Shoppinglists");
        }
    }
}
