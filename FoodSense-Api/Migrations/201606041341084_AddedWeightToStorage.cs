namespace FoodSense_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedWeightToStorage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StorageHistories", "Weight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StorageHistories", "Weight");
        }
    }
}
