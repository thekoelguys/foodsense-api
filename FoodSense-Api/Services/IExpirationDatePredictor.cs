﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Services
{
    interface IExpirationDatePredictor
    {
        DateTime PredictExpirationDate(Product product);
    }
}
