﻿using System.Collections.Generic;
using FoodSense_Api.Models;

namespace RecommendationSystem.Services
{
    public interface IRecipeRecommendationService
    {
        ICollection<Recipe> GetRecipesForStorage(ICollection<RecipeRating> recipeRatings, ICollection<StoragesProducts> productsInStorage);
    }
}