﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.Core.Internal;
using FoodSense_Api.Enums;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Services
{
    public class AchievementService
    {
        private readonly IRepository<Achievement> _achievementRepository;
        private readonly IRepository<UserAchievement> _userAchievementRepository;

        public AchievementService(BaseApplicationDbContext ctx)
        {
            _achievementRepository = new AchievementRepository(ctx);
            _userAchievementRepository = new UserAchievementRepository(ctx);
        }

        public List<Achievement> CheckForAchievements(ApplicationUser user, AchievementType type, int count)
        {
            List<Achievement> achievedAchievements = new List<Achievement>();
            ICollection<Achievement> achievements  = _achievementRepository.GetAll(a => a.AchievementType == type);

            foreach (Achievement achievement in achievements)
            {
                if (count >= achievement.Points && IsNotAchievedByUser(user, achievement))
                {
                    achievedAchievements.Add(achievement);
                    UserAchievement userAchievement = new UserAchievement(user, achievement);

                    _userAchievementRepository.Insert(userAchievement);
                    user.UserAchievements.Add(userAchievement);
                    achievement.UserAchievements.Add(userAchievement);
                    _userAchievementRepository.Save();
                }
            }

            return achievedAchievements;
        }

        private bool IsNotAchievedByUser(ApplicationUser user, Achievement achievement)
        {
            return user.UserAchievements.FirstOrDefault(ua => ua.AchievementId == achievement.Id) == null;
        }
    }
}