﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace RecommendationSystem.Services
{
    /// <summary>
    /// This service can give you recommended recipes based on what is in your storage and what is rated in the past by the user.
    /// </summary>
    public class RecipeRecommendationService : IRecipeRecommendationService
    {
        /// <summary>
        /// This Service gives a list of recipes based on the retings in the past
        /// </summary>
        /// <param name="recipeRatings">Recipe ratings to predict possible recipes</param>
        /// <param name="productsInStorage">Products who are in storage</param>
        /// <returns></returns>
        public ICollection<Recipe> GetRecipesForStorage(ICollection<RecipeRating> recipeRatings, ICollection<StoragesProducts> productsInStorage)
        {
            Dictionary<int, ProductWithRating> RatedProducts = GetProductWoAreRatedByAnRecipe(recipeRatings);

            ICollection<Recipe> RecommendedRecipes = new List<Recipe>();

            foreach (KeyValuePair<int, ProductWithRating> entry in RatedProducts)
            {
                bool isInStorage = true;
                if (productsInStorage != null)
                {
                    isInStorage = (productsInStorage.Where(p => p.ProductId == entry.Key).ToList().Count > 0);
                }

                foreach (Recipe recipe in entry.Value.Product.Recipes)
                {
                    if (!RecommendedRecipes.Contains(recipe))
                    {
                        recipe.AverageRating = isInStorage ? entry.Value.Rating + 2 : entry.Value.Rating - 1;
                        RecommendedRecipes.Add(recipe);
                    }
                    else
                    {
                        Recipe res = RecommendedRecipes.FirstOrDefault(r => r.Id == recipe.Id);
                        res.AverageRating = isInStorage ? res.AverageRating + 2 : res.AverageRating - 1;
                    }
                }
            }


            return RecommendedRecipes.OrderByDescending(a => a.AverageRating).ToList();
        }

        private Dictionary<int, ProductWithRating> GetProductWoAreRatedByAnRecipe(ICollection<RecipeRating> recipeRatings)
        {
            Dictionary<int, ProductWithRating> RatedProducts = new Dictionary<int, ProductWithRating>();
            foreach (RecipeRating rating in recipeRatings)
            {
                if(rating.Recipe == null)
                {
                    continue;
                }
                foreach (Product product in rating.Recipe.Ingredients)
                {
                    if (RatedProducts.ContainsKey(product.Id))
                    {
                        ProductWithRating ratedProduct = RatedProducts[product.Id];
                        ratedProduct.AddUser(rating.Rating);
                    }
                    else
                    {
                        ProductWithRating ratedProduct = new ProductWithRating(product);
                        ratedProduct.AddUser(rating.Rating);
                        RatedProducts.Add(product.Id, ratedProduct);
                    }
                }
            }
            return RatedProducts;
        }

        private class ProductWithRating
        {
            public ProductWithRating(Product product)
            {
                Product = product;
            }

            public Product Product { get; private set; }
            public int Rating { get; private set; }
            public int NumberOfUsers { get; private set; }
            private int _total;

            public void AddUser(int rating)
            {
                NumberOfUsers++;
                _total = _total + rating;
                if (Rating > 0)
                    Rating = _total / NumberOfUsers;
                else
                    Rating = rating;
            }
        }

    }
}
