﻿using System;
using System.Linq;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Services
{
    public class ExpirationDatePredictor : IExpirationDatePredictor
    {
        private readonly IRepository<StoragesProducts> _storageProductRepository;

        public ExpirationDatePredictor(IRepository<StoragesProducts> storageProductRepository)
        {
            _storageProductRepository = storageProductRepository;
        }

        public DateTime PredictExpirationDate(Product product)
        {
            var storagesProducts = _storageProductRepository.GetAll(s => s.ProductId == product.Id);

            if (storagesProducts.Count <= 0) return DateTime.Now.AddDays(3);

            double sumDays = storagesProducts.Sum(storageProduct => storageProduct.ExpirationDate.Subtract(storageProduct.DateAdded).TotalDays);
            double avgDayDifference = Math.Floor(sumDays / storagesProducts.Count);

            return DateTime.Now.AddDays(avgDayDifference);
        }
    }


}