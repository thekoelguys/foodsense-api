﻿namespace FoodSense_Api.Enums
{
    public enum AchievementType
    {
        Recipe,
        AddingProducts,
        DeletedProducts
    }
}