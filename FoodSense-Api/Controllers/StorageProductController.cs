﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FoodSense_Api.Enums;
using FoodSense_Api.Models;
using FoodSense_Api.Models.BindingModels.Product;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Services;

namespace FoodSense_Api.Controllers
{

   [Authorize]
    public class StorageProductController : BaseApiController
    {
        private readonly AbstractRepository<Product> _productRepository;
        private readonly AbstractRepository<StoragesProducts> _storagesProductsRepository;
        private readonly AchievementService _achievementService;

       public StorageProductController()
       {
            _storagesProductsRepository = new StoragesProductsRepository(Context);
            _productRepository = new ProductRepository(Context);
            _achievementService = new AchievementService(Context);
        }

        public StorageProductController(BaseApplicationDbContext ctx)
        {
            _storagesProductsRepository = new StoragesProductsRepository(ctx);
            _productRepository = new ProductRepository(ctx);
            _achievementService = new AchievementService(ctx);
        }

        /// <summary> Get all products that the user has in his storage. </summary>
        /// <response code="200"> Request success! </response>
        [ResponseType(typeof(ICollection<StoragesProducts>))]
        public IHttpActionResult Get()
        {
            var list = new List<StoragesProducts>();
            foreach (var storage in LoggedInUser.Storages)
            {
                list.AddRange(storage.StorageProducts);
            }

            return Ok(list);
        }

        /// <summary> Get information about a single StorageProduct. </summary>
        /// <param name="storageId">Id of the Storage </param>
        /// <response code="200" > Request success! </response>
        /// <response code="404"> No StorageProduct found. </response>
        [Route("api/StorageProduct/id/{storageProductId}")]
        [ResponseType(typeof(ICollection<StoragesProducts>))]
        public IHttpActionResult GetByStorageProductId(int storageProductId)
        {
            StoragesProducts storageProduct = LoggedInUser.Storages
                .SelectMany(s => s.StorageProducts)
                .FirstOrDefault(sp => sp.Id == storageProductId);

            if (storageProduct == null)
                return NotFound();

            return Ok(storageProduct);
        }

        /// <summary> Get all products from a storage.  </summary>
        /// <param name="storageId">Id of the Storage </param>
        /// <response code="200" > Request success! </response>
        /// <response code="404"> No storage found. </response>
        [Route("api/StorageProduct/{storageId}")]
        [ResponseType(typeof(ICollection<StoragesProducts>))]
        public IHttpActionResult Get(int storageId)
        {
            Storage storage = LoggedInUser.Storages.FirstOrDefault(s => s.Id == storageId);

            if (storage == null)
                return NotFound();

            ICollection<StoragesProducts> storagesProducts = _storagesProductsRepository.GetAll(sp => sp.StorageId == storageId);

            return Ok(storagesProducts);
        }

        /// <summary> Add a product a to a storage. Achievements can be sent back in this call. Read docs for more information. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid fields. </response>
        /// <response code="404"> No storage found OR No user found. </response>
        [ResponseType(typeof(List<Achievement>))]
        public IHttpActionResult Post(AddProductModel addProductModel)
        {
            Storage storage = LoggedInUser.Storages.FirstOrDefault(s => s.Id == addProductModel.StorageId);
            Product product = _productRepository.GetById(addProductModel.ProductId);

            if (storage == null || product == null)
                return NotFound();

            for (int i = 0; i < addProductModel.Amount; i++)
            {
                _storagesProductsRepository.Insert(new StoragesProducts(storage, product,
                    DateTime.ParseExact(addProductModel.ExpirationDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)));

                LoggedInUser.AddedProductCount++;
            }

            _storagesProductsRepository.Save();

            List<Achievement> achievedAchievements = _achievementService
                .CheckForAchievements(LoggedInUser, AchievementType.AddingProducts, LoggedInUser.AddedProductCount);

            return Ok(achievedAchievements.Count == 0 ? null : achievedAchievements);
        }

        /// <summary> Delete a product from a storage. Achievements can be sent back in this call. Read docs for more information. </summary>
        /// <param name="id">Id of the StorageProduct </param>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid parameter type. </response>
        /// <response code="404"> No storage found OR No user found. </response>
        [HttpDelete]
        [Route("api/StorageProduct/{id}")]
        public IHttpActionResult Delete(int id)
        {
            StoragesProducts storageProduct = LoggedInUser.Storages
                .SelectMany(s => s.StorageProducts)
                .FirstOrDefault(sp => sp.Id == id);

            if (storageProduct == null)
                return NotFound();

            if (DateTime.Now <= storageProduct.ExpirationDate)
                LoggedInUser.DeletedProductInTimeCount++;

            List<Achievement> achievedAchievements = _achievementService
                .CheckForAchievements(LoggedInUser, AchievementType.DeletedProducts, LoggedInUser.DeletedProductInTimeCount);

            _storagesProductsRepository.Delete(storageProduct);
            _storagesProductsRepository.Save();

            return Ok(achievedAchievements.Count == 0 ? null : achievedAchievements);
        }

        /// <summary> Update a product from a storage. </summary>
        /// <param name="id"> Id of the StorageProduct </param>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> No storage found OR No user found. </response>
        [HttpPatch]
        [Route("api/StorageProduct/{id}")]
        public IHttpActionResult Patch(int id, UpdateStorageProductModel updateStorageProduct)
        {
            StoragesProducts storageProduct = _storagesProductsRepository.GetById(id);

            if (storageProduct == null)
                return NotFound();
            
            storageProduct.ExpirationDate = DateTime.ParseExact(updateStorageProduct.ExpirationDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            storageProduct.Weight = updateStorageProduct.Weight;
            storageProduct.WeightType = updateStorageProduct.WeightType;
            storageProduct.Weight = updateStorageProduct.Weight;

            _storagesProductsRepository.Save();

            return Ok();
        }

        /// <summary>
        /// Gives info of specified storage
        /// </summary>
        /// <param name="connectionToken"></param>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> No storage found OR No user found. </response>
        [AllowAnonymous]
        [Route("api/StorageProduct/getProductsInfoOfStorage/{connectionToken}")]
        [ResponseType(typeof(StorageInfo))]
        public IHttpActionResult GetStorageInfo(string connectionToken)
        {
            ICollection<StoragesProducts> storagesProducts = _storagesProductsRepository.GetAll(s => s.Storage.ConnectionToken == connectionToken);
            if(storagesProducts == null)
                return NotFound();

            IExpirationDatePredictor predictor = new ExpirationDatePredictor(_storagesProductsRepository);

            StorageInfo storageInfo = new StorageInfo();
            storageInfo.NumberOfProducts = storagesProducts.Count;
            storageInfo.firstExpiringProductDate = DateTime.Now.AddYears(100);

            foreach (StoragesProducts storageProduct in storagesProducts)
            {
                DateTime predictedExpirationDate = predictor.PredictExpirationDate(storageProduct.Product);
                if(predictedExpirationDate < storageInfo.firstExpiringProductDate)
                {
                    storageInfo.firstExpiringProductDate = predictedExpirationDate;
                    storageInfo.firstExpiringProduct = storageProduct.Product;
                }
            }

            return Ok(storageInfo);
        }
    }
}