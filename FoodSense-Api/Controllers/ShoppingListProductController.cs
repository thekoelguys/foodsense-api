﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FoodSense_Api.Models;
using FoodSense_Api.Models.BindingModels.Shoppinglist;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Services;

namespace FoodSense_Api.Controllers
{
    [Authorize]
    public class ShoppingListProductController : BaseApiController
    {
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Shoppinglist> _shoppingListRepository;
        private readonly IRepository<ShoppinglistProduct> _shoppingListProductRepository;
        private readonly IRepository<StoragesProducts> _storageProductRepository;

        public ShoppingListProductController()
        {
            _productRepository = new ProductRepository(Context);
            _shoppingListRepository = new ShoppingListRepository(Context);
            _shoppingListProductRepository = new ShoppinglistProductRepository(Context);
            _storageProductRepository = new StoragesProductsRepository(Context);
        }

        public ShoppingListProductController(BaseApplicationDbContext applicationDbContext)
        {
            _productRepository = new ProductRepository(applicationDbContext);
            _shoppingListRepository = new ShoppingListRepository(applicationDbContext);
            _shoppingListProductRepository = new ShoppinglistProductRepository(applicationDbContext);
            _storageProductRepository = new StoragesProductsRepository(applicationDbContext);
        }

        /// <summary> Get all products that the user has on his shoppinglists. </summary>
        /// <response code="200"> Request success! </response>
        [ResponseType(typeof(ICollection<ShoppinglistProduct>))]
        public IHttpActionResult Get()
        {
            List<ShoppinglistProduct> list = new List<ShoppinglistProduct>();

            foreach (var shoppinglist in LoggedInUser.Shoppinglists)
            {
                list.AddRange(shoppinglist.ShoppinglistsProducts);
            }

            return Ok(list);
        }

        /// <summary> Get all products from a shoppinglist. </summary>
        /// <param name="shoppingListId">Id of the Shoppinglist </param>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> No shoppinglist found. </response>
        [Route("api/ShoppinglistProduct/{shoppingListId}")]
        [ResponseType(typeof(ICollection<ShoppinglistProduct>))]
        public IHttpActionResult Get(int shoppingListId)
        {
            Shoppinglist shoppinglist = LoggedInUser.Shoppinglists.FirstOrDefault(s => s.Id == shoppingListId);

            if (shoppinglist == null)
                return NotFound();

            ICollection<ShoppinglistProduct> shoppinglistProducts = _shoppingListProductRepository.GetAll(sp => sp.ShoppingListId == shoppinglist.Id);

            return Ok(shoppinglistProducts);
        }

        /// <summary> Call for adding a product to the shoppinglist. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid fields in the request body. </response>
        /// <response code="404"> Product not found. </response>
        public IHttpActionResult Post(AddToShoppingListModel shoppingListModel)
        {
            Shoppinglist shoppinglist = LoggedInUser.Shoppinglists.FirstOrDefault(s => s.Id == shoppingListModel.ShoppingListId);

            if (shoppinglist == null)
                return NotFound();

            Product product = _productRepository.Get(p => p.Id == shoppingListModel.ProductId);

            if (product == null)
                return NotFound();

            for (int i = 0; i < shoppingListModel.Amount; i++)
            {
                ShoppinglistProduct shoppinglistProduct = new ShoppinglistProduct(product, shoppinglist);
                shoppinglist.ShoppinglistsProducts.Add((shoppinglistProduct));
                product.ShoppinglistsProducts.Add(shoppinglistProduct);
            }

            _shoppingListRepository.Save();

            return Ok();
        }

        /// <summary> Call for deleting a product out of a shoppinglist</summary>
        /// <param name="id">Id of the ShoppinglistProduct </param>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> ShoppinglistProduct not found. </response>
        [Route("api/ShoppingListProduct/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            ShoppinglistProduct shoppinglistProduct = LoggedInUser.Shoppinglists
                .SelectMany(s => s.ShoppinglistsProducts)
                .FirstOrDefault(sp => sp.Id == id);
        
            if (shoppinglistProduct == null)
                return NotFound();

            _shoppingListProductRepository.Delete(shoppinglistProduct);
            _shoppingListProductRepository.Save();

            return Ok();
        }

        /// <summary> Call for exporting shoppinglistproducts to a storage. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> Product not found. </response>
        [HttpPost]
        [Route("api/ShoppinglistProduct/export")]
        public IHttpActionResult ExportProductsToStorage(ExportShoppinglistModel exportShoppinglistModel)
        {
            Storage storage = LoggedInUser.Storages
                .FirstOrDefault(s => s.Id == exportShoppinglistModel.StorageId);

            if (storage == null)
                return NotFound();

            IExpirationDatePredictor predictor = new ExpirationDatePredictor(_storageProductRepository);

            foreach (int productId in exportShoppinglistModel.Products)
            {
                Product product = _productRepository.GetById(productId);

                if (product == null)
                    return NotFound();

                StoragesProducts storagesProducts = new StoragesProducts(storage, product,
                    predictor.PredictExpirationDate(product));

                _storageProductRepository.Insert(storagesProducts);
                storage.StorageProducts.Add(storagesProducts);
                product.StorageProducts.Add(storagesProducts);
            }

            _storageProductRepository.Save();

            return Ok();
        }
    }
}