﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Controllers
{
    [Authorize]
    public class ProductController : BaseApiController
    {
        private readonly IRepository<Product> _productRepository;

        public ProductController()
        {
            _productRepository = new ProductRepository(Context);
        }

        public ProductController(BaseApplicationDbContext ctx)
        {
            _productRepository = new ProductRepository(ctx);
        }

        /// <summary> Get all products. </summary>
        /// <response code="200"> Request success! </response>
        public ICollection<Product> Get()
        {
            return _productRepository.GetAll();
        }

        /// <summary> Get product information by product id.  </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> No product found. </response>
        [ResponseType(typeof(Product))]
        public IHttpActionResult Get(int id)
        {
            Product product = _productRepository.GetById(id);

            if (product == null)
                return NotFound();

            return Ok(product);
        }

        /// <summary> Call for adding new products to the database. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid fields. </response>
        /// <response code="409"> Product already exists. </response>
        [ResponseType(typeof(Product))]
        public IHttpActionResult Post(Product insertProduct)
        {
            Product product = _productRepository.Get(p => p.EAN == insertProduct.EAN);

            if (product != null)
                return Conflict();

            _productRepository.Insert(insertProduct);
            _productRepository.Save();

            return Ok(insertProduct);
        }

        /// <summary> Get product information by product EAN.  </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> No product found. </response>
        [Route("api/Product/ean/{ean}")]
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetByEan(string ean)
        {
            Product product = _productRepository.Get(p => p.EAN == ean);

            if (product == null)
                return NotFound();

            return Ok(product);
        }
    }
}
