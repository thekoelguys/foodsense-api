﻿using FoodSense_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FoodSense_Api.Enums;
using FoodSense_Api.Models.BindingModels.Recipe;
using RecommendationSystem;
using FoodSense_Api.Repositories.Implementations;
using RecommendationSystem.Services;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Services;

namespace FoodSense_Api.Controllers
{
    [Authorize]
    public class RecipeRecommendationController : BaseApiController
    {
        private readonly IRepository<RecipeRating> _recipeRecommendationRepository;
        private readonly IRepository<Recipe> _recipeRepository;
        private readonly AchievementService _achievementService;

        public RecipeRecommendationController()
        {
            _recipeRecommendationRepository = new RecipeRecommendationRepository(Context);
            _achievementService = new AchievementService(Context);
            _recipeRepository = new RecipeRepository(Context);
        }

        private readonly IRecipeRecommendationService _recommendationService = new RecipeRecommendationService();

        /// <summary> Get recommended recipes. </summary>
        /// <response code="200"> Request success! </response>
        public ICollection<Recipe> Get()
        {
            IRepository<StoragesProducts> storagesProductsRepository = new StoragesProductsRepository(Context);
            List<int> storageIds = new List<int>();
            LoggedInUser.Storages.ToList().ForEach(s => storageIds.Add(s.Id));
            ICollection<StoragesProducts> storagesProducts= storagesProductsRepository.GetAll(sp => storageIds.Contains(sp.StorageId));

            ICollection<RecipeRating> recipeRatings = _recipeRecommendationRepository.GetAll(u => u.User.Id == LoggedInUser.Id);
            return _recommendationService.GetRecipesForStorage(recipeRatings, storagesProducts);
        }

        /// <summary> Rate a recipe. Achievements can be sent back in this call. Read docs for more information. </summary>
        /// <response code="200"> Request success! </response>
        [ResponseType(typeof(List<Achievement>))]
        public IHttpActionResult Post(RateRecipeModel rateRecipeModel)
        {
            Recipe recipe = _recipeRepository.GetById(rateRecipeModel.RecipeId);            

            if(recipe == null)
                return NotFound();

            RecipeRating recipeRating = _recipeRecommendationRepository.Get(r => r.User.Id == LoggedInUser.Id && r.Recipe.Id == rateRecipeModel.RecipeId);
            ICollection<RecipeRating> recipeRatings =
                _recipeRecommendationRepository.GetAll(r => r.Recipe.Id == recipe.Id);

            if (recipeRating == null)
            {
                recipeRating = new RecipeRating()
                {
                    Recipe = recipe,
                    Rating = rateRecipeModel.Rating,
                    User = LoggedInUser
                };

                recipe.RecipeRatings.Add(recipeRating);
                _recipeRecommendationRepository.Insert(recipeRating);
            }
            else
            {
                recipeRating.Rating = rateRecipeModel.Rating;
                _recipeRecommendationRepository.Update(recipeRating);
            }
            recipe.AverageRating = recipeRatings.Sum(r => r.Rating) / recipeRatings.Count;

            LoggedInUser.RecipeRatedCount++;
            _recipeRecommendationRepository.Save();

            List<Achievement> achievedAchievements = _achievementService
                .CheckForAchievements(LoggedInUser, AchievementType.Recipe, LoggedInUser.RecipeRatedCount);

            return Ok(achievedAchievements.Count == 0 ? null : achievedAchievements);
        }
    }
}
