﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FoodSense_Api.Models;
using FoodSense_Api.Models.BindingModels.Product;
using FoodSense_Api.Models.BindingModels.Storage;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Controllers
{
    [Authorize]
    public class StorageController : BaseApiController
    {
        private readonly IRepository<Storage> _storageRepository;
        private readonly IRepository<StorageHistory> _storageHistoryRepository;

        public StorageController()
        {
            _storageRepository        = new StorageRepository(Context);
            _storageHistoryRepository = new StorageHistoryRepository(Context);
        }

        /// <summary> Retrieves all storages that are linked to the logged in user. </summary>
        /// <response code="200"> Request success! </response>
        [Route("api/Storage")]
        public ICollection<Storage> GetStorages()
        {
            return LoggedInUser.Storages;
        }

        /// <summary> Call for registering a storage - First Step </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid post body! </response>
        /// <response code="404"> Storage not found! </response>
        [AllowAnonymous]
        [HttpPost]
        [Route("api/Storage/Register")]
        public IHttpActionResult RegisterStorage(StorageRegisterModel storageRegisterModel)
        {
            Storage storage = _storageRepository.Get(s => s.RegistrationToken == storageRegisterModel.Token);

            if (storage != null) return Ok(storage.ConnectionToken);

            storage = new Storage(storageRegisterModel.Token);
            _storageRepository.Insert(storage);
            _storageRepository.Save();

            return Ok(storage.ConnectionToken);
        }

        /// <summary> Call for registering a storage - Second Step </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid post body </response>
        /// <response code="404"> Storage not found! </response>
        /// <response code="409"> Storage already belongs to the logged in user. </response>
        [HttpPost]
        [Route("api/Storage/Add")]
        [ResponseType(typeof(Storage))]
        public IHttpActionResult AddStorageToUser(StorageAddModel storageAddModel)
        {
            Storage storage = _storageRepository.Get(s => s.ConnectionToken == storageAddModel.Token);

            if (storage == null)
                return NotFound();
            
            if (storage.Users.FirstOrDefault(u => u.Id == LoggedInUser.Id) != null)
                return Conflict();

            storage.Name = storageAddModel.Name;

            storage.Users.Add(LoggedInUser);
            LoggedInUser.Storages.Add(storage);
            _storageRepository.Save();

            return Ok(storage);
        }

        /// <summary> Get information about a specific storage </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> Storage not found! </response>
        [ResponseType(typeof(Storage))]
        [Route("api/Storage/{id}")]
        public IHttpActionResult GetStorageById(int id)
        {
            Storage storage = LoggedInUser.Storages.FirstOrDefault(s => s.Id == id);

            if (storage == null)
                return NotFound();

            return Ok(storage);
        }

        /// <summary> Delete a storage </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> Storage not found. This also happens when the storage exists, but the user has no access.  </response>
        [HttpDelete]
        [Route("api/Storage/{id}")]
        public IHttpActionResult DeleteStorageById(int id)
        {
            Storage storage = LoggedInUser.Storages.FirstOrDefault(s => s.Id == id);

            if (storage == null)
                return NotFound();

            _storageRepository.Delete(storage);
            _storageRepository.Save();

            return Ok();
        }

        /// <summary> Add a new StorageHistory (Contains sensor-data) to the storage. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid request body! </response>
        /// <response code="404"> Storage not found. This also happens when the storage exists, but the user has no access.  </response>
        [HttpPost]
        [AllowAnonymous]
        [Route("api/Storage")]
        [ResponseType(typeof(Storage))]
        public IHttpActionResult UpdateStorage(string connectionToken, [FromBody] UpdateStorageModel updateStorageModel)
        {
            Storage storage = _storageRepository.Get(s => s.ConnectionToken == connectionToken);

            if (storage == null)
                return NotFound();

            storage.Humidity = updateStorageModel.Humidity;
            storage.Temperature = updateStorageModel.Temperature;
            storage.Weight = updateStorageModel.Weight;
            _storageRepository.Save();

            StorageHistory storageHistory = new StorageHistory(updateStorageModel.Humidity, updateStorageModel.Temperature, updateStorageModel.Weight, storage);
            _storageHistoryRepository.Insert(storageHistory);
            _storageHistoryRepository.Save();

            return Ok(storage);
        }        
    }
}
