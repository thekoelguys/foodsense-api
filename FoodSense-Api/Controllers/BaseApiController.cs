﻿using System.Web.Http;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.AspNet.Identity;

namespace FoodSense_Api.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        private readonly IRepository<ApplicationUser> _userRepository;
        private ApplicationUser _loggedInUser;

        public BaseApiController()
        {
            Context         = new ApplicationDbContext();
            _userRepository = new UserRepository(Context);
        }

        public BaseApplicationDbContext Context { get; }

        public ApplicationUser LoggedInUser
        {
            get
            {
                string userId = User.Identity.GetUserId();
                return _loggedInUser ?? _userRepository.Get(u => u.Id == userId);
            }
        }
    }
}
