﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using FoodSense_Api.Models;
using FoodSense_Api.Models.BindingModels;
using FoodSense_Api.Models.BindingModels.Account;

namespace FoodSense_Api.Controllers
{
    public class AccountController : BaseApiController
    {
        private ApplicationUserManager _userManager;
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        public AccountController(){}

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        
        /// <summary> Register a user. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid fields. </response>
        [AllowAnonymous]
        [Route("api/register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Name = model.Name,
                Email = model.Email,
                Age = model.Age,
                Sex = model.Sex,
            };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            return !result.Succeeded ? GetErrorResult(result) : Ok();
        }

        /// <summary> Change the password of the user. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid fields. </response>
        [Authorize]
        [Route("api/changePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            
            return !result.Succeeded ? GetErrorResult(result) : Ok();
        }


        /// <summary> Get the user profile </summary>
        /// <response code="200"> Request success! </response>
        [Authorize]
        [Route("api/profile")]
        [ResponseType(typeof(Profile))]
        public IHttpActionResult GetProfile()
        {
            return Ok(LoggedInUser.GetProfile(true));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
