﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FoodSense_Api.Models;
using FoodSense_Api.Models.BindingModels.Shoppinglist;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Controllers
{
    [Authorize]
    public class ShoppingListController : BaseApiController
    {
        private readonly IRepository<Shoppinglist> _shoppingListRepository;

        public ShoppingListController()
        {
            _shoppingListRepository = new ShoppingListRepository(Context);
        }

        public ShoppingListController(BaseApplicationDbContext applicationDbContext)
        {
            _shoppingListRepository        = new ShoppingListRepository(applicationDbContext);
        }

        /// <summary>
        ///  Call for getting all shoppinglists that belong to the user.
        /// </summary>
        /// <response code="200"> Request success! </response>
        [Route("api/Shoppinglist")]
        [ResponseType(typeof(ICollection<Shoppinglist>))]
        public IHttpActionResult GetShoppinglists()
        {
            return Ok(LoggedInUser.Shoppinglists);
        }

        /// <summary>
        ///  Call for getting a Shoppinglist by id.
        /// </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> Shopping list not found for the user. </response>
        [Route("api/Shoppinglist/{id}")]
        [ResponseType(typeof(Shoppinglist))]
        public IHttpActionResult Get(int id)
        {
            Shoppinglist shoppinglist = LoggedInUser.Shoppinglists.FirstOrDefault(s => s.Id == id);

            if (shoppinglist == null)
                return NotFound();

            return Ok(shoppinglist);
        }


        /// <summary> Call for creating a new shoppinglist. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid fields in the request body. </response>
        /// <response code="404"> Product not found. </response>
        [HttpPost]
        [Route("api/Shoppinglist")]
        [ResponseType(typeof(Shoppinglist))]
        public IHttpActionResult Post(NewShoppingListModel newShoppingListModel)
        {
            Shoppinglist shoppinglist = new Shoppinglist(LoggedInUser, newShoppingListModel.Name);

            _shoppingListRepository.Insert(shoppinglist);
            LoggedInUser.Shoppinglists.Add(shoppinglist);
            _shoppingListRepository.Save();

            return Ok(shoppinglist);
        }

        /// <summary> Call for deleting a shoppinglist. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> Product not found. </response>
        [HttpDelete]
        [Route("api/Shoppinglist/{id}")]
        public IHttpActionResult Delete(int id)
        {
            Shoppinglist shoppinglist = LoggedInUser.Shoppinglists.FirstOrDefault(s => s.Id == id);

            if (shoppinglist == null)
                return NotFound();

            _shoppingListRepository.Delete(shoppinglist);
            _shoppingListRepository.Save();

            return Ok();
        }
    }
}
