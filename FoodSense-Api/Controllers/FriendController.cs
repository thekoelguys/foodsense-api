﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FoodSense_Api.Models;
using FoodSense_Api.Models.BindingModels.Friend;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Controllers
{
    [Authorize]
    public class FriendController : BaseApiController
    {
        private readonly IRepository<ApplicationUser> _userRepository;

        public FriendController()
        {
            _userRepository = new UserRepository(Context);
        }

        /// <summary> Get a list of all user profiles. </summary>
        /// <response code="200"> Request success! </response>
        [Route("api/User")]
        public IEnumerable<Profile> GetAllPotentialFriends()
        {
            ICollection<ApplicationUser> users = _userRepository.GetAll();

            return users.Select(user => user.GetProfile(false));
        }

        /// <summary> Get all friends. </summary>
        /// <response code="200"> Request success! </response>
        public ICollection<Profile> Get()
        {
            return LoggedInUser.Friends.Select(friend => friend.GetProfile(false)).ToList();
        }


        /// <summary> Add a friend. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid post body </response>
        /// <response code="409"> User is already friends with this user. </response>
        public IHttpActionResult Post([FromBody] string friendid)
        {
            ApplicationUser friend = _userRepository.Get(u => u.Id == friendid);

            if (friend == null)
                return NotFound();

            if (LoggedInUser.Friends.FirstOrDefault(f => f.Id == friendid) != null)
                return Conflict();

            LoggedInUser.Friends.Add(friend);
            friend.Friends.Add(LoggedInUser);

            _userRepository.Save();

            return Ok(friend.GetProfile(false));
        }

        /// <summary> Delete a friend. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="400"> Invalid post body </response>
        public IHttpActionResult Delete(string id)
        {
            ApplicationUser friend = LoggedInUser.Friends.FirstOrDefault(f => f.Id == id);

            if (friend == null)
                return NotFound();

            LoggedInUser.Friends.Remove(friend);
            _userRepository.Save();

            return Ok();
        }
    }
}
