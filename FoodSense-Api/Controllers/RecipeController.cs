﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using FoodSense_Api.Enums;
using FoodSense_Api.Models;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Services;

namespace FoodSense_Api.Controllers
{
    [Authorize]
    public class RecipeController : BaseApiController
    {
        private readonly IRepository<Recipe> _recipeRepository;

        public RecipeController()
        {
            _recipeRepository = new RecipeRepository(Context);
        }

        /// <summary> Get all recipes. </summary>
        /// <response code="200"> Request success! </response>
        public ICollection<Recipe> Get()
        {
            return _recipeRepository.GetAll();
        }
    
        /// <summary> Get information about a specific recipe. </summary>
        /// <response code="200"> Request success! </response>
        /// <response code="404"> Recipe not found. </response>
        [ResponseType(typeof(Recipe))]
        public IHttpActionResult Get(int id)
        {
            Recipe recipe = _recipeRepository.GetById(id);

            if (recipe == null)
                return NotFound();

            return Ok(recipe);
        }
    }
}
