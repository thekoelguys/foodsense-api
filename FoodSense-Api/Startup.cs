﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FoodSense_Api.Models;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(FoodSense_Api.Startup))]

namespace FoodSense_Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
