﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;

namespace FoodSense_Api.Repositories.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Insert(T entity);
        T GetById(int id);
        T Get(Expression<Func<T, bool>> where);
        ICollection<T> GetAll();
        ICollection<T> GetAll(Expression<Func<T, bool>> where);
        void Update(T entity);
        void Delete(T entity);
        void Save();
    }
}
