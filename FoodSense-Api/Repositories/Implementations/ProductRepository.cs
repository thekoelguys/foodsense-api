﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FoodSense_Api.Repositories.Implementations
{
    public class ProductRepository : AbstractRepository<Product>
    {

        public ProductRepository(BaseApplicationDbContext context) : base(context)
        {
                                
        }
  
    }
}