﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Repositories.Implementations
{
    public class ShoppingListRepository : AbstractRepository<Shoppinglist>
    {
        public ShoppingListRepository(BaseApplicationDbContext context) : base(context)
        {
        }
    }
}