﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Repositories.Implementations
{
    public class StoragesProductsRepository : AbstractRepository<StoragesProducts>
    {
        private readonly BaseApplicationDbContext _context;

        public StoragesProductsRepository(BaseApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public override ICollection<StoragesProducts> GetAll(Expression<Func<StoragesProducts, bool>> @where)
        {
            return _context.Set<StoragesProducts>()
                .Include("Product")
                .Where(where).ToList();
        }
    }
}