﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Models;

namespace FoodSense_Api.Repositories.Implementations
{
    public class RecipeRecommendationRepository : AbstractRepository<RecipeRating>
    {
        private readonly BaseApplicationDbContext _context;

        public RecipeRecommendationRepository(BaseApplicationDbContext context) : base(context)
        {
            _context = context;   
        }

        public override ICollection<RecipeRating> GetAll(Expression<Func<RecipeRating, bool>> @where)
        {
            return _context.Set<RecipeRating>()
                .Include("Recipe")
                .Include("Recipe.Ingredients")
                .Include("Recipe.Ingredients.Recipes")
                .Where(where).ToList();
        }
    }
}