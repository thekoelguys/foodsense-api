﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;

namespace FoodSense_Api.Repositories.Implementations
{
    public class AchievementRepository : AbstractRepository<Achievement>
    {
        public AchievementRepository(BaseApplicationDbContext context) : base(context)
        {
        }
    }
}