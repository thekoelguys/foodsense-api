﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FoodSense_Api.Repositories.Implementations
{
    public class UserRepository : AbstractRepository<ApplicationUser>
    {
        public UserRepository(BaseApplicationDbContext context) : base(context)
        {
        }
    }
}