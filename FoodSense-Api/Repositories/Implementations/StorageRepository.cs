﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Repositories
{
    public class StorageRepository : AbstractRepository<Storage>
    {
        public StorageRepository(BaseApplicationDbContext context) : base(context)
        {
        }
    }
}