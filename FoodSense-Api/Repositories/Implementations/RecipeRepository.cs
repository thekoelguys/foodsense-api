﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;
using FoodSense_Api.Models;

namespace FoodSense_Api.Repositories.Implementations
{
    public class RecipeRepository : AbstractRepository<Recipe>
    {
        private readonly BaseApplicationDbContext _context;

        public RecipeRepository(BaseApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public override Recipe GetById(int id)
        {
            return _context.Recipes
                .Include("Ingredients")
                .FirstOrDefault(r => r.Id == id);
        }
        public override ICollection<Recipe> GetAll()
        {
            return _context.Set<Recipe>()
                .Include("Ingredients")
                .ToList();
        }
    }
}