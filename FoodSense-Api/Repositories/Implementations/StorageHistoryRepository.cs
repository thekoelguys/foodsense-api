﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Implementations;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Repositories
{
    public class StorageHistoryRepository : AbstractRepository<StorageHistory>
    {
        public StorageHistoryRepository(BaseApplicationDbContext context) : base(context)
        {
        }

    }
}