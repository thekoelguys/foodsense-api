﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using FoodSense_Api.Models;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Repositories.Implementations
{
    public class ShoppinglistProductRepository : AbstractRepository<ShoppinglistProduct>
    {
        public ShoppinglistProductRepository(BaseApplicationDbContext context) : base(context)
        {
        }
    }
}