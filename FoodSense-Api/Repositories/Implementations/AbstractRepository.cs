﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using FoodSense_Api.Models.Context;
using FoodSense_Api.Repositories.Interfaces;

namespace FoodSense_Api.Repositories.Implementations
{
    public abstract class AbstractRepository<T> : IRepository<T> where T : class
    {
        private readonly BaseApplicationDbContext _context;

        protected AbstractRepository(BaseApplicationDbContext context)
        {
            _context = context;
        }

        public virtual void Insert(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public virtual T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public virtual T Get(Expression<Func<T, bool>> @where)
        {
            return _context.Set<T>().FirstOrDefault(where);
        }

        public virtual ICollection<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public virtual ICollection<T> GetAll(Expression<Func<T, bool>> @where)
        {
            return _context.Set<T>().Where(where).ToList();
        }

        public virtual void Update(T entity)
        {
            _context.Entry<T>(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }
    }
}