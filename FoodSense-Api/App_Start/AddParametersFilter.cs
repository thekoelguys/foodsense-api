﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace FoodSense_Api.App_Start
{
    public class AddParametersFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            // Check for authorize attribute.
            var scopes = apiDescription.ActionDescriptor.GetFilterPipeline()
                .Select(filterInfo => filterInfo.Instance)
                .OfType<AuthorizeAttribute>()
                .SelectMany(attr => attr.Roles.Split(','))
                .Distinct();

            if (scopes.Any())
            {
                if(operation.parameters == null)
                    operation.parameters = new List<Parameter>();

                // Authorization header - Set only when Authorization is required.
                operation.parameters.Add(new Parameter()
                {
                    description = "Authorization token. Token url : POST /api/login username={UserNameorEmailAddress}&password={YourPassword}&grant_type=password",
                    @in = "header",
                    name = "Authorization",
                    @default = "Bearer ",
                    required = true,
                    type = "string"
                });
            }

            // Content-Type header.
            operation.parameters.Add(new Parameter()
            {
                description = "Content-Type, use either application/json or application/xml.",
                @in = "header",
                name = "Content-Type",
                @default = "application/json",
                required = true,
                type = "string"
            });

            // Accept header.
            operation.parameters.Add(new Parameter()
            {
                description = "Accept header, use either application/json or application/xml.",
                @in = "header",
                name = "Accept",
                @default = "application/json",
                required = true,
                type = "string"
            });
        }
    }
}